
//  Copyright © 2014 IV-Tech. All rights reserved.
//  www.iv-tech.com

package cn.eight.utils;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Data package for discovery and control devices
 * Packet format
 *
 * Packet Header1
 * Packet Header1
 * PAYLOAD_LENGTH(length of command_id + length of command_data)
 * command_id
 * Command_data
 * Checksum
 */

public class Command
{
    private static final int CONTROL_PACKET = 0x1001;
    private static final int CONTROL_OP_BROWSE_LIST = 0x3002;   //Search command ID
    public static InetAddress masterAddress;                    //Multicast address
    public static final int REAL_MULTICAST_PORT = 1234;         //Multicast port

    private static final int CONTROL_HEADER_1 = 0xFF;           //Packet head one
    private static final int CONTROL_HEADER_2 = 0x55;           //Packet head two
    private static final int CONTROL_NEXT = 0x01;               //next track
    private static final int CONTROL_PREVIOUS = 0x02;           //previous track
    private static final int CONTROL_PLAY_PAUSE = 0x03;         //play/pause
    private static final int CONTROL_VOLUME_PLUS = 0x04;        //Plus Volume
    private static final int CONTROL_VOLUME_MINUS = 0x05;       //Minus Volume
    private static final int CONTROL_STOP = 0X06;               //Stop play
    private static final int CONTROL_GETVOLUME = 0x07;          //Get Volume
    private static final int CONTROL_GET_PLAYBACK_STATUS = 0x08;//Get playback
    private static final int CONTROL_SETVOLUME = 0x0D;          //Set volume
    private static final int CONTROL_GET_PLAYBACK_INFO = 0x13;  //Get playback info
    private static final int CONTROL_GET_VERSION = 0x47;        //Get firmware version

    private static final byte PAYLOAD_LENGTH1 = 1;//Payload length=1
    private static final byte PAYLOAD_LENGTH2 = 2;//Payload length=2

    /**
     * Search device, through the multicast  to send
     * @return DatagramPacket
     * @throws UnknownHostException
     */
    public static DatagramPacket cmdBrowse() throws UnknownHostException
    {
        ByteBuffer byteBuffer = ByteBuffer.allocate(12);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer = byteBuffer.putInt(CONTROL_PACKET);
        byteBuffer = byteBuffer.putInt(CONTROL_OP_BROWSE_LIST);
        byteBuffer = byteBuffer.putInt(0);

        byte[] outputBuffer = byteBuffer.array();

        return new DatagramPacket(outputBuffer, outputBuffer.length, masterAddress, REAL_MULTICAST_PORT);
    }

    /**
     * next track
     * @return byte[]
     */
    public static byte[] cmdNext()
    {

        return buidDataPacket((byte) CONTROL_NEXT);
    }

    /**
     * previous track
     * @return byte[]
     */
    public static byte[] cmdPrev()
    {
        return buidDataPacket((byte) CONTROL_PREVIOUS);
    }

    /**
     * Continue playing／pause
     * @return byte[]
     */
    public static byte[] cmdPlayPause()
    {
        return buidDataPacket((byte) CONTROL_PLAY_PAUSE);
    }

    /**
     * play track
     * @param playURL track url
     * @return byte[]
     */
    public static byte[] cmdPlay(String playURL)
    {
        return buidDataPacket((byte) CONTROL_PLAY_PAUSE, playURL);
    }

    /**
     * stop playing
     * @return byte[]
     */
    public static byte[] cmdStopSpeaker()
    {
        return buidDataPacket((byte) CONTROL_STOP);
    }

    /**
     * Plus Volume
     * @return byte[]
     */
    public static byte[] cmdVolumePlus() throws UnknownHostException
    {
        return buidDataPacket((byte) CONTROL_VOLUME_PLUS);
    }

    /**
     * Minus Volume
     * @return byte[]
     */
    public static byte[] cmdVolumeMinus()
    {
        return buidDataPacket((byte) CONTROL_VOLUME_MINUS);
    }

    /**
     * Set volume
     * @param volume volume value
     * @return byte[]
     */
    public static byte[] cmdSetVolume(int volume)
    {
        return buidDataPacket((byte) CONTROL_SETVOLUME, volume);

    }

    /**
     * Get Volume
     * @return byte[]
     */
    public static byte[] cmdGetVolume()
    {
        return buidDataPacket((byte) CONTROL_GETVOLUME);
    }

    /**
     * Get playback info
     * @return byte[]
     */
    public static byte[] cmdPlaybackStatus()
    {
        return buidDataPacket((byte) CONTROL_GET_PLAYBACK_STATUS);
    }

    /**
     * Get playback info
     * @return byte[]
     */
    public static byte[] cmdGetPlaybackInfo()
    {
        return buidDataPacket((byte) CONTROL_GET_PLAYBACK_INFO);
    }

    /**
     * Get version info(Both the firmware version and hardware type)
     * @return byte[]
     */
    public static byte[] cmdGetVersion()
    {
        return buidDataPacket((byte) CONTROL_GET_VERSION);
    }


    /**
     * Build packet
     * @param commandId command id
     * @return new byte[]
     */
    private static byte[] buidDataPacket(byte commandId)
    {
        byte[] dataPacket = new byte[]{(byte) CONTROL_HEADER_1, (byte) CONTROL_HEADER_2, PAYLOAD_LENGTH1, commandId};
        return new byte[]{(byte) CONTROL_HEADER_1, (byte) CONTROL_HEADER_2, (byte) PAYLOAD_LENGTH1, commandId, checksumGenerator(
                dataPacket)};
    }

    /**
     * Build packet
     * @param commandId   command id
     * @param commandData command data
     * @return new byte[]
     */
    private static byte[] buidDataPacket(byte commandId, int commandData)
    {
        byte[] dataPacket = new byte[]{(byte) CONTROL_HEADER_1, (byte) CONTROL_HEADER_2, PAYLOAD_LENGTH2, commandId, (byte) commandData};
        return new byte[]{(byte) CONTROL_HEADER_1, (byte) CONTROL_HEADER_2, PAYLOAD_LENGTH2, commandId, (byte) commandData, checksumGenerator(
                dataPacket)};
    }

    /**
     * Build packet
     * @param commandId   command id
     * @param commandData command data
     * @return new byte[]
     */
    private static byte[] buidDataPacket(byte commandId, String commandData)
    {
        int dataLength = commandData.getBytes().length;//length of commandData
        ByteBuffer byteBuffer = ByteBuffer.allocate(300);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.put((byte) CONTROL_HEADER_1);//Packet head one
        byteBuffer.put((byte) CONTROL_HEADER_2);//Packet head two
        byteBuffer.put((byte) (dataLength + 1));//length of payload
        byteBuffer.put(commandId);//command id
        byteBuffer.put(commandData.getBytes());

        byte[] dataPacket = byteBuffer.array();
        byteBuffer.put(checksumGenerator(dataPacket));

        return byteBuffer.array();
    }

    /**
     * Get checksum
     * @param commandPacket The packet of need to Calculation
     * @return byte
     */
    private static byte checksumGenerator(byte[] commandPacket)
    {
        byte checksum = 0x0;

        for(int loop = 2; loop < commandPacket.length; loop++)
        {
            checksum -= commandPacket[loop];
        }
        return checksum;
    }
}
