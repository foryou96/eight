package cn.eight.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import cn.eight.R;


public class Ripple extends RelativeLayout {
    private static final int DEFAULT_RIPPLE_COUNT = 6;
    private static final int DEFAULT_DURATION_TIME = 3000;
    private static final float DEFAULT_SCALE = 6.0f;
    private static final int DEFAULT_FILL_TYPE = 0;

    //波纹是否在动
    private boolean animationRunning = false;
    //波纹颜色
    private int rippleColor;
    //画笔的宽度
    private float rippleStrokeWidth;
    //波纹半径
    private float rippleRadius;
    //波纹持续时间
    private int rippleDurationTime;
    //波纹数量
    private int rippleAmount;
    //波纹放大
    private float rippleScale;
    //创建画笔
    private Paint paint;
    //实现波纹动画集合
    AnimatorSet animatorSet1, animatorSet2, animatorSet3, animatorSet4, animatorSet5, animatorSet6, animatorSet7, animatorSet8;

    private ArrayList<RippleView> rippleViewList = new ArrayList<RippleView>();

    public Ripple(Context context) {
        super(context);
    }

    public Ripple(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);

        initPainter();

        initAtrrs(context, attrs);

    }

    public Ripple(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);

        initPainter();

        initAtrrs(context, attrs);
    }

    private void init(final Context context, final AttributeSet attrs) {
        if (isInEditMode())
            return;

        initPainter();

        initAtrrs(context, attrs);

    }

    //初始化画笔
    public void initPainter() {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(rippleColor);
    }

    //设置属性
    private void initAtrrs(Context context, AttributeSet attrs) {
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Ripple);
        rippleColor = typedArray.getColor(R.styleable.Ripple_rb_color, getResources().getColor(R.color.rippelColor));
        rippleStrokeWidth = typedArray.getDimension(R.styleable.Ripple_rb_strokeWidth, getResources().getDimension(R.dimen.rippleStrokeWidth));
        rippleRadius = typedArray.getDimension(R.styleable.Ripple_rb_radius, getResources().getDimension(R.dimen.rippleRadius));
        rippleDurationTime = typedArray.getInt(R.styleable.Ripple_rb_duration, DEFAULT_DURATION_TIME);
        rippleAmount = typedArray.getInt(R.styleable.Ripple_rb_rippleAmount, DEFAULT_RIPPLE_COUNT);
        rippleScale = typedArray.getFloat(R.styleable.Ripple_rb_scale, DEFAULT_SCALE);
        int rippleType = typedArray.getInt(R.styleable.Ripple_rb_type, DEFAULT_FILL_TYPE);
        typedArray.recycle();
    }

    //创建动画
    public void aniset(int id) {
        switch (id) {
            case 0:
                animatorSet1 = new AnimatorSet();
                animatorSet1.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 1:
                animatorSet2 = new AnimatorSet();
                animatorSet2.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 2:
                animatorSet3 = new AnimatorSet();
                animatorSet3.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 3:
                animatorSet4 = new AnimatorSet();
                animatorSet4.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 4:
                animatorSet5 = new AnimatorSet();
                animatorSet5.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 5:
                animatorSet6 = new AnimatorSet();
                animatorSet6.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 6:
                animatorSet7 = new AnimatorSet();
                animatorSet7.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            case 7:
                animatorSet8 = new AnimatorSet();
                animatorSet8.setInterpolator(new AccelerateDecelerateInterpolator());
                break;
            default:
                break;
        }
    }

    public void draw(int id, float x, float y, int wh) {
        aniset(id);
        //产生波纹的时间间隔
        int rippleDelay = rippleDurationTime / rippleAmount;
        LayoutParams rippleParams = new LayoutParams((int) (2 * (rippleRadius + rippleStrokeWidth)), (int) (2 * (rippleRadius + rippleStrokeWidth)));
        //初始化动画
        ArrayList<Animator> animatorList = new ArrayList<Animator>();

        for (int i = 0; i < rippleAmount; i++) {
            RippleView rippleView = new RippleView(getContext(), x, y, wh);
            addView(rippleView, -1, rippleParams);
            rippleViewList.add(rippleView);
            final ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(rippleView, "ScaleX", 1.0f, rippleScale);
            scaleXAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            scaleXAnimator.setRepeatMode(ObjectAnimator.RESTART);
            scaleXAnimator.setStartDelay(i * rippleDelay);
            scaleXAnimator.setDuration(rippleDurationTime);
            animatorList.add(scaleXAnimator);
            final ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(rippleView, "ScaleY", 1.0f, rippleScale);
            scaleYAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            scaleYAnimator.setRepeatMode(ObjectAnimator.RESTART);
            scaleYAnimator.setStartDelay(i * rippleDelay);
            scaleYAnimator.setDuration(rippleDurationTime);
            animatorList.add(scaleYAnimator);
            final ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(rippleView, "Alpha", 1.0f, 0f);
            alphaAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            alphaAnimator.setRepeatMode(ObjectAnimator.RESTART);
            alphaAnimator.setStartDelay(i * rippleDelay);
            alphaAnimator.setDuration(rippleDurationTime);
            animatorList.add(alphaAnimator);
        }
        switch (id) {
            case 0:
                animatorSet1.playTogether(animatorList);
                break;
            case 1:
                animatorSet2.playTogether(animatorList);
                break;
            case 2:
                animatorSet3.playTogether(animatorList);
                break;
            case 3:
                animatorSet4.playTogether(animatorList);
                break;
            case 4:
                animatorSet5.playTogether(animatorList);
                break;
            case 5:
                animatorSet6.playTogether(animatorList);
                break;
            case 6:
                animatorSet7.playTogether(animatorList);
                break;
            case 7:
                animatorSet8.playTogether(animatorList);
                break;
            default:
                break;
        }
    }

    private class RippleView extends View {
        private int wh;

        public RippleView(Context context) {
            super(context);
            this.setVisibility(View.INVISIBLE);
        }

        public RippleView(Context context, float x, float y, int wh) {
            super(context);
            this.setVisibility(View.INVISIBLE);
            this.wh = wh;
            setX(x);
            setY(y - Utils.getStatusBarHeight());
        }

        protected void onDraw(Canvas canvas) {
            float radius = wh / 2;
            canvas.drawCircle(radius, radius, radius - rippleStrokeWidth, paint);
        }
    }

    public void startRippleAnimation(boolean flag, int id) {
        if (flag) {
            for (RippleView rippleView : rippleViewList) {
                rippleView.setVisibility(VISIBLE);
            }
            switch (id) {
                case 0:
                    animatorSet1.start();
                    break;
                case 1:
                    animatorSet2.start();
                    break;
                case 2:
                    animatorSet3.start();
                    break;
                case 3:
                    animatorSet4.start();
                    break;
                case 4:
                    animatorSet5.start();
                    break;
                case 5:
                    animatorSet6.start();
                    break;
                case 6:
                    animatorSet7.start();
                    break;
                case 7:
                    animatorSet8.start();
                    break;
                default:
                    break;
            }
        }
    }

    public void stopRippleAnimation(boolean flag, int id) {
        if (!flag) {
            switch (id) {
                case 0:
                    animatorSet1.end();
                    System.out.println("end");
                    break;
                case 1:
                    animatorSet2.end();
                    break;
                case 2:
                    animatorSet3.end();
                    break;
                case 3:
                    animatorSet4.end();
                    break;
                case 4:
                    animatorSet5.end();
                    break;
                case 5:
                    animatorSet6.end();
                    break;
                case 6:
                    animatorSet7.end();
                    break;
                case 7:
                    animatorSet8.end();
                    break;
                default:
                    break;
            }
        }
    }
}
