package cn.eight.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class MyImg extends ImageView {
    private ObjectAnimator rot; // 循环旋转动画
    private ObjectAnimator rot1;// 补间动画 当未旋转到0°时 需要用这个动画来平缓过度到0°
    private float pauseValue; // 最后一次 暂停时的角度
    private boolean isRot1Cancle = false; // 是否是在平滑动画的时候点击的暂停 /**

    public MyImg(Context context) {
        super(context);
    }

    public MyImg(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyImg(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    //暂停或者继续的操作
    public void speaksAnim() {
        if (rot1 == null) { // 表示 从未执行过动画 即为第一次进入
            statAnimator();
            return;
        }
        if (rot1.isRunning()) {
            isRot1Cancle = true;
            rot1.cancel();
        } else {
            if (rot == null) {
                statAnimator();
            } else {
                if (rot.isRunning()) {
                    rot.cancel();
                } else {
                    statAnimator();
                }
            }
        }
    }

    /**
     * 开始补间动画
     */
    private void statAnimator() {
        rot1 = ObjectAnimator.ofFloat(this, "rotation", pauseValue, 360);
        rot1.setDuration((long) ((360 - pauseValue) * 6000 / 360));
        rot1.setRepeatCount(0);
        rot1.setInterpolator(new LinearInterpolator());
        rot1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                pauseValue = (float) animation.getAnimatedValue();
            }
        });
        rot1.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator animation) {
                isRot1Cancle = false;
            }

            public void onAnimationEnd(Animator animation) {
                if (!isRot1Cancle) {
                    startRepatAnim();
                }
            }

            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationRepeat(Animator animation) {

            }
        });
        if (pauseValue == 360) {
            startRepatAnim();
        } else {
            rot1.start();
        }

    }

    /**
     * 开始循环动画
     */
    private void startRepatAnim() {
        rot = ObjectAnimator.ofFloat(this, "rotation", 0, 360);
        rot.setDuration(6000);
        rot.setRepeatCount(-1);
        rot.setInterpolator(new LinearInterpolator());
        rot.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                pauseValue = (float) animation.getAnimatedValue();
            }
        });
        rot.start();

    }

}
