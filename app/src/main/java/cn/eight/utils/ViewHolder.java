package cn.eight.utils;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import cn.eight.R;

public class ViewHolder {
    public TextView textView;
    public CheckBox checkBox;
    public ViewHolder(View view){
        textView= (TextView) view.findViewById(R.id.tv_listview);
        checkBox= (CheckBox) view.findViewById(R.id.checkBox_listview);
    }
}
