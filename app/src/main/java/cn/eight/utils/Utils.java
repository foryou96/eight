package cn.eight.utils;


public class Utils {
    public static int StatusBarHeight;
    private int[][] position = new int[8][2];

    public static int getStatusBarHeight() {
        return StatusBarHeight;
    }

    public static void setStatusBarHeight(int statusBarHeight) {
        Utils.StatusBarHeight = statusBarHeight;
    }

    public int[][] getPosition() {
        return position;
    }

    public void setPosition(int[][] position) {
        this.position = position;
    }
}