
//  Copyright © 2014 IV-Tech. All rights reserved.
//  www.iv-tech.com

package cn.eight.utils;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * when you send browse command ,if found device ,the MulticastSocket will receive data,and you can call handleMulticastCommandReceived(byte[] receivedData)
 * to handle the receive data
 *
 * when your APP establish TCP connection with the speaker,and the receive thread receive data,you can call  handleTCPCommandReceived(byte[] receivedData)
 * to handle the receive data.
 *
 * playback status can received when playback status change or you call cmdPlaybackStatus()
 * playback info can received when skip track or you call cmdGetPlaybackInfo()
 * version info can received when  you call cmdGetVersion()
 * device info  can received when  you call cmdGetVersion()
 *
 * In order to guarantee you can find device,you should make your app join in 239.11.11.11 and port is 1234
 * In order to guarantee you can receive playing progress ,you should make your app join in 239.11.11.10 and port is 1234
 *
 * On TCP connection, the data format you send is the same as the data you receive
 */
public class Response
{

    public final static int RES_BROWSE_LIST = 0x3004;                                   //browse result response id
    private static final int REAL_NAME_LENGTH = 32;                                     //length of speaker/group name
    public final static int RES_SONG_PROGRESS = 0x5007;                                 //playback progress response id
    public final static int RES_ACTION_ACK = 0x3100;                                    //acknowledgement response id
    public final static int CNTL_PB_HTTP_FINISHED = 0x502e;                             //playback finish response

    //for playback status
    public final static byte RES_PLAYBACK_STATUS = (byte) 0x41;                         //playback status response id
    public final static byte RES_PLAYBACKSTATUS_IDLE_READY_STOP = (byte) 0x01;          //returned when speaker is in idle/ready/stop state
    public final static byte RES_PLAYBACKSTATUS_AIRPLAY_IS_WORKING = (byte) 0x02;       //returned when airplay is working (returned with current playback progress)
    public final static byte RES_PLAYBACKSTATUS_DLNA_IS_WORKING = (byte) 0x03;          //returned when dlna is working (returned with current playback progress)
    public final static byte RES_PLAYBACKSTATUS_PAUSE = (byte) 0x04;                    //returned when speaker is paused (returned with current playback progress)
    public final static byte RES_PLAYBACKSTATUS_CURRENT_VOLUME = (byte) 0x05;           //returned together with current volume
    public final static byte RES_PLAYBACKSTATUS_MAX_VOLUME = (byte) 0x06;               //returned when speaker reaches max volume
    public final static byte RES_PLAYBACKSTATUS_MIN_VOLUME = (byte) 0x07;               //returned when speaker reaches min volume
    public final static byte RES_PLAYBACKSTATUS_STREAMING_IS_WORKING = (byte) 0x08;     //returned when speaker is streaming music
    public final static byte RES_PLAYBACKSTATUS_PLAYBACK_POSITION = (byte) 0x09;        //returned with the current playback time (only works when speaker is streaming online music)
    public final static byte RES_PLAYBACKSTATUS_PLAYBACK_FINISHED = (byte) 0x0a;        //returned when playing is over (only works when speaker is streaming online music)
    public final static byte RES_PLAYBACKSTATUS_ONLINE_MUSIC_IS_WORKING = (byte) 0x0b;  //returned when speaker is streaming online music (returned with current playback progress)
    public final static byte RES_PLAYBACKSTATUS_LOCALPLAY_IS_WORKING = (byte) 0x0c;     //returned when speaker is streaming local music (e.g playing music on SD card, also returns with
    //current playback progress)
    //for playback info
    public final static byte RES_PLAYBACKINFO = (byte) 0x53;                            //playback info response id
    public final static byte RES_PLAYBACKINFO_ARTIST = (byte) 0x01;                     //returned with current track artist
    public final static byte RES_PLAYBACKINFO_ALBUM = (byte) 0x02;                      //returned with current track album
    public final static byte RES_PLAYBACKINFO_ALBUM_URL = (byte) 0x03;                  //returned with current album url
    public final static byte RES_PLAYBACKINFO_DURATION = (byte) 0x04;                   //returned with current track total duration
    public final static byte RES_PLAYBACKINFO_TITLE = (byte) 0x05;                      //returned with current track title
    public final static byte RES_PLAYBACKINFO_URl = (byte) 0x06;                        //returned with current track playback url

    public final static byte RES_VERSION_INFO = (byte) 0x48;                            //firmware version response id
    public final static byte RES_DEVINFO = (byte) 0x54;                                 //device version info response id



    /**
     * handle multicast data
     *
     * @param receivedData receive data
     */
    public void handleMulticastCommandReceived(byte[] receivedData)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(receivedData);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int packetType = byteBuffer.getInt();
        int multiOperationalCode = byteBuffer.getInt();  //get command id or opcode
        int bodyLength = byteBuffer.getInt();

        //check if the response id(aka opcode) received
        if(multiOperationalCode == RES_BROWSE_LIST)
        {
            int valid = byteBuffer.getInt();            //if valid > 0 means it is a valid command
            long id = byteBuffer.getLong();             //get speaker id
            long groupId = byteBuffer.getLong();        //get group id (for multiroom only)
            int type = byteBuffer.getInt();

            int address = byteBuffer.getInt();          //get group address (for multiroom only)
            int port = byteBuffer.getInt();             //get group port (for multiroom only)
            int hostIp = byteBuffer.getInt();           //get speaker ip

            if(isAbnormalIP(hostIp))
            {
                return;
            }

            byte[] nameBuffer = new byte[REAL_NAME_LENGTH];

            for(int i = 0; i < REAL_NAME_LENGTH; i++)
            {
                nameBuffer[i] = byteBuffer.get();
            }

            //getting the speaker name

            String speakerName = null;

            try
            {
                speakerName = new String(nameBuffer, "UTF-8");
                speakerName = speakerName.trim();
            }
            catch(UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }

            // Establish tcp connection here with speaker with the speaker ip parsed.
        }

        else if(multiOperationalCode == RES_SONG_PROGRESS)
        {
            long playbackTime = byteBuffer.getLong();    //get track playing progress
        }

        else if(multiOperationalCode == RES_ACTION_ACK)
        {
            int seq = byteBuffer.getInt();           //sequence number sent together with command to speaker (used to check whether the command sent is received by the speaker)
        }

        else if(multiOperationalCode == CNTL_PB_HTTP_FINISHED)
        {
            long playbackTime = byteBuffer.getLong();
        }
    }

    /**
     * handle TCP data
     *
     * @param receivedData the data of receive
     */
    public void handleTCPCommandReceived(byte[] receivedData)
    {
        if(receivedData[0] == (byte) 0xff && receivedData[1] == (byte) 0x55)
        {
            try
            {
                //check for the response id(aka opcode)
                switch(receivedData[3])
                {
                    case RES_PLAYBACK_STATUS:
                    {
                        decodePlaybackStatus(receivedData);
                    }
                    break;

                    case RES_PLAYBACKINFO:
                    {
                        decodePlaybackInfo(receivedData);
                    }
                    break;

                    case RES_VERSION_INFO:
                    {
                        decodeVersionInfo(receivedData);
                    }
                    break;

                    case RES_DEVINFO:
                    {
                        decodeDevinfo(receivedData);
                    }
                    break;
                }
            }

            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    /**
     * handle playback status
     *
     * @param receivedData receive data
     */
    private void decodePlaybackStatus(byte[] receivedData)
    {
        long progress = 0L;
        switch(receivedData[4])
        {
            case RES_PLAYBACKSTATUS_IDLE_READY_STOP:
            {
                //handle events when speaker is in idle/ready/stop state
            }
            break;

            case RES_PLAYBACKSTATUS_AIRPLAY_IS_WORKING:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);

                //handle events when airplay is working
            }
            break;

            case RES_PLAYBACKSTATUS_DLNA_IS_WORKING:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);

                //handle events dlna is working
            }
            break;

            case RES_PLAYBACKSTATUS_STREAMING_IS_WORKING:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);

                //handle events when streaming is working
            }
            break;

            case RES_PLAYBACKSTATUS_ONLINE_MUSIC_IS_WORKING:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);

                //handle events when online is working
            }
            break;

            case RES_PLAYBACKSTATUS_LOCALPLAY_IS_WORKING:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);

                //handle events when local play is working
            }
            break;

            case RES_PLAYBACKSTATUS_PAUSE:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);

                //handle events when speaker is paused
            }
            break;

            case RES_PLAYBACKSTATUS_CURRENT_VOLUME:
            {
                int volume = receivedData[5];
            }
            break;

            case RES_PLAYBACKSTATUS_MAX_VOLUME:
            {
                //handle events when max volume is reached
            }
            break;

            case RES_PLAYBACKSTATUS_MIN_VOLUME:
            {
                //handle events when min volume is reached
            }
            break;

            case RES_PLAYBACKSTATUS_PLAYBACK_POSITION:
            {
                byte[] playbackProgress = new byte[8];

                System.arraycopy(receivedData, 5, progress, 0, 8);
            }
            break;

            case RES_PLAYBACKSTATUS_PLAYBACK_FINISHED:
            {
                //handle events when playback is finished
            }
            break;
        }
    }

    /**
     * handle playback info
     *
     * @param receivedData receive data
     */
    private void decodePlaybackInfo(byte[] receivedData)
    {
        //We do this step because received data may contain garbages and we need to get rid of them.
        int realDataLength = 0;

        for(int x = 0; x < receivedData.length; x++)
        {
            if(receivedData[x] == 0x00)
            {
                realDataLength = x - 5; //5 = length of (commandHead1+commandHead2+dataLength+commandId+checksum)

                break;
            }
        }

        switch(receivedData[4])
        {
            case RES_PLAYBACKINFO_ARTIST:
            {
                byte[] infoBytes = new byte[realDataLength];
                System.arraycopy(receivedData, 5, infoBytes, 0, realDataLength);

                String infoString = null;

                try
                {
                    infoString = new String(infoBytes, "UTF-8");
                }

                catch(UnsupportedEncodingException e)
                {
                    e.printStackTrace();

                    break;
                }
            }
            break;

            case RES_PLAYBACKINFO_ALBUM:
            {
                byte[] infoBytes = new byte[realDataLength];
                System.arraycopy(receivedData, 5, infoBytes, 0, realDataLength);

                String infoString = null;

                try
                {
                    infoString = new String(infoBytes, "UTF-8");
                }

                catch(UnsupportedEncodingException e)
                {
                    e.printStackTrace();

                    break;
                }
            }
            break;

            case RES_PLAYBACKINFO_ALBUM_URL:
            {
                byte[] infoBytes = new byte[realDataLength];
                System.arraycopy(receivedData, 5, infoBytes, 0, realDataLength);
                String infoString = new String(infoBytes).replaceAll("[^\\p{ASCII}]", "");
            }
            break;

            case RES_PLAYBACKINFO_DURATION:
            {
                long duration = 0;
                duration = receivedData[12] << 8 | receivedData[11] << 8 | receivedData[10] << 8 | receivedData[9] << 8 | receivedData[8] << 8 | receivedData[7] << 8 | receivedData[6] << 8 | receivedData[5] & 0xff;
            }
            break;

            case RES_PLAYBACKINFO_TITLE:
            {
                byte[] infoBytes = new byte[realDataLength];
                System.arraycopy(receivedData, 5, infoBytes, 0, realDataLength);

                String infoString = null;
                try
                {
                    infoString = new String(infoBytes, "UTF-8");
                }

                catch(UnsupportedEncodingException e)
                {
                    e.printStackTrace();

                    break;
                }

                Log.v("ResponseDecoder", "decodePlaybackInfo: TITLE:" + infoString);
            }
            break;

            case RES_PLAYBACKINFO_URl:
            {
                byte[] infoBytes = new byte[realDataLength];
                System.arraycopy(receivedData, 5, infoBytes, 0, realDataLength);
                String infoString = new String(infoBytes).replaceAll("[^\\p{ASCII}]", "");
            }
            break;

            default:
                break;
        }
    }

    /**
     * handle firmware info
     */
    private void decodeVersionInfo(byte[] receivedData)
    {
        byte[] messageContent = new byte[receivedData.length - 4 - 1];
        System.arraycopy(receivedData, 4, messageContent, 0, messageContent.length);
        String versionInfo = new String(messageContent);
    }


    /**
     * handle device info
     *
     * @param receivedData receive data
     */
    public void decodeDevinfo(byte[] receivedData)
    {

        int dataLength = receivedData[2] - 1;
        byte[] hardwareTypeBytes = new byte[dataLength];
        System.arraycopy(receivedData, 4, hardwareTypeBytes, 0, dataLength);

        String hardwareInfo = new String(hardwareTypeBytes);
    }

    public static boolean isAbnormalIP(int ipAddress)
    {
        if(ipAddress == 0)
        {
            return true;
        }

        else
        {
            String ipString = intToIp(ipAddress);
            String lastTwoValueString = new String("0.0");
            String subIpString = ipString.substring(ipString.length() - 3, ipString.length());
            return subIpString.equals(lastTwoValueString);
        }
    }

    public static String intToIp(int i)
    {
        return (i & 255) + "." + (i >> 8 & 255) + "." + (i >> 16 & 255) + "." + (i >> 24 & 255);
    }
}
