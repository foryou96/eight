package cn.eight.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;
public class RotatAnimation {

    private ObjectAnimator rot; // 循环旋转动画
    private ObjectAnimator rot1;// 补间动画 当未旋转到0°时 需要用这个动画来平缓过度到0°
    private boolean isAnimationRunning = false; // 动画是否在运行
    private float pauseValue; // 最后一次 暂停时的角度
    private boolean isRot1Cancle = false; // 是否是在平滑动画的时候点击的暂停
    private View view;

    public RotatAnimation(View view) {
        this.view = view;
    }

    /**
     * 开始动画
     */
    public void startAnimation() {
        statAnimator();
    }

    public boolean isAnimationRunning() {
        return isAnimationRunning;
    }

    public void pauseAnimation() {
        isAnimationRunning = false;
        if (rot1.isRunning()) {
            isRot1Cancle = true;
            rot1.cancel();
        } else if (rot.isRunning()) {
            rot.cancel();
        }
    }

    /**
     * 开始补间动画
     */
    private void statAnimator() {
        isAnimationRunning = true;
        rot1 = ObjectAnimator.ofFloat(view, "rotation", pauseValue, 360);
        rot1.setDuration((long) ((360 - pauseValue) * 5000 / 360));
        rot1.setRepeatCount(0);
        rot1.setInterpolator(new LinearInterpolator());
        rot1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                pauseValue = (float) animation.getAnimatedValue();
                //System.out.println("Login1Activity.onAnimationUpdate pausevalue" + pauseValue);
            }
        });
        rot1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isRot1Cancle = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isRot1Cancle) {
                    startRepatAnim();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        if (pauseValue == 360) {
            startRepatAnim();
        } else {
            rot1.start();
        }

    }

    /**
     * 开始循环动画
     */
    private void startRepatAnim() {
        rot = ObjectAnimator.ofFloat(view, "rotation", 0, 360);
        rot.setDuration(5000);
        rot.setRepeatCount(-1);
        rot.setInterpolator(new LinearInterpolator());
        rot.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                pauseValue = (float) animation.getAnimatedValue();
            }
        });
        rot.start();
    }
}
