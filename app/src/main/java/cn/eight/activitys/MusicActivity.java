package cn.eight.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.eight.R;
import cn.eight.basic.MediaScanner;
import cn.eight.basic.MusicService;
import cn.eight.utils.Audio;
import cn.eight.utils.MediaUtils;
import cn.eight.utils.ViewHolder;
import cn.eight.utils.ViewHolderlist;

public class MusicActivity extends Activity {
    private MediaScanner mediaScanner;
    private ListView listView;
    private List<Audio> musciList;
    private static Context mContext;
    private TextView tv_nomusic;
    public static boolean notemty=false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        init();
        searchMusic();
        sendList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (musciList!=null){
                    Intent intent = new Intent(mContext, MusicService.class);
                    // /设置初始化指令
                    intent.putExtra(MusicService.KEY_COMMAND, MusicService.CMD_PLAY);
                    // 传递播放位置
                    intent.putExtra(MusicService.KEY_MUSIC_INDEX, i);
                    // 启动服务
                    startService(intent);
                }
            }
        });
    }

    private void sendList(){
        // 给服务设置播放列表
        Intent intent = new Intent(mContext, MusicService.class);

        // 解析播放列表，只获取播放路径
        ArrayList<String> music = new ArrayList<String>();
        for (Audio audio : musciList) {
            music.add(audio.getPath());
        }
        // 设置初始化指令
        intent.putExtra(MusicService.KEY_COMMAND,
                MusicService.CMD_INIT);

        // 传递初始化数据
        intent.putExtra(MusicService.KEY_MUSIC_LIST, music);

        // 启动服务
        startService(intent);
    }

    public void searchMusic(){
        mediaScanner = new MediaScanner(mContext);
        File file = new File("/mnt");
        mediaScanner.scanFile(file, "flac");
        musciList = MediaUtils.getAudioList(mContext);
        if (musciList.size() == 0) {
            listView.setVisibility(View.GONE);
            tv_nomusic.setVisibility(View.VISIBLE);
        } else {
            MusicAdapter musicAdapter = new MusicAdapter(mContext, musciList);
            listView.setAdapter(musicAdapter);
            notemty=true;
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        mediaScanner.disconnect();
    }

    private void init(){
        mContext=this;
        listView = (ListView) findViewById(R.id.lv_musiclist);
        tv_nomusic = (TextView) findViewById(R.id.tv_nomusic);
    }

    public void searchMusic(View view) {
        searchMusic();
    }

    private class MusicAdapter extends BaseAdapter {
        private List<Audio> list;
        private Context context;
        private LayoutInflater layoutInflater;

        public MusicAdapter(Context context, List<Audio> list) {
            this.context = context;
            this.list = list;
            layoutInflater = LayoutInflater.from(this.context);
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int i) {
            return list.get(i);
        }

        public long getItemId(int i) {
            return i;
        }

        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolderlist viewHolderlist = null;
            View view;
            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.layout_musiclist, null);
                viewHolderlist = new ViewHolderlist(view);
                view.setTag(viewHolderlist);
            } else {
                view = convertView;
                viewHolderlist = (ViewHolderlist) view.getTag();
            }
            viewHolderlist.textView.setText(list.get(i).getTitle()+" - "+list.get(i).getArtist());
            return view;
        }
    }
}
