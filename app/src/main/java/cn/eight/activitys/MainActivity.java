package cn.eight.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import cn.eight.basic.MusicService;
import cn.eight.basic.MusicUtil;
import cn.eight.utils.MyImg;
import cn.eight.R;
import cn.eight.utils.Ripple;
import cn.eight.utils.Utils;


public class MainActivity extends AppCompatActivity {
    private ImageButton bt_setting;
    private ImageView iv_music_recond;
    private ImageView iv_play_pause;
    private MyImg iV1, iV2, iV3, iV4, iV5, iV6, iV7, iV8;
    private TextView textView;
    private SeekBar seekBar;
    private Context mContext;
    private Utils mUtils;
    private int wh;
    private int[][] position;
    private int IdFlag = 0;
    private boolean idFlag1 = false, idFlag2 = false, idFlag3 = false, idFlag4 = false, idFlag5 = false, idFlag6 = false, idFlag7 = false, idFlag8 = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        initView();
        initsharedPreferences();
        changePlay();

        textView.setText(mContext.getResources().getString(R.string.vol) + "0%");

//        iV1.speaksAnim();
//        iV2.speaksAnim();
//        iV3.speaksAnim();
//        iV4.speaksAnim();
//        iV5.speaksAnim();
//        iV6.speaksAnim();
//        iV7.speaksAnim();
//        iV8.speaksAnim();

        iV1.setOnTouchListener(new OnTouchRot());
        iV2.setOnTouchListener(new OnTouchRot());
        iV3.setOnTouchListener(new OnTouchRot());
        iV4.setOnTouchListener(new OnTouchRot());
        iV5.setOnTouchListener(new OnTouchRot());
        iV6.setOnTouchListener(new OnTouchRot());
        iV7.setOnTouchListener(new OnTouchRot());
        iV8.setOnTouchListener(new OnTouchRot());
        bt_setting.setOnClickListener(new Setting());
        iv_music_recond.setOnClickListener(new Setting());


        /**
         * 进度条百分比
         */
        seekBar.setMax(100);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textView.setText(mContext.getResources().getString(R.string.vol) + i + "%");
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void changePlay() {
        if (MusicUtil.isPlaying) {
            iv_play_pause.setBackgroundResource(R.drawable.music_pause);
        } else {
            iv_play_pause.setBackgroundResource(R.drawable.music_play);
        }
    }

    protected void onRestart() {
        super.onRestart();
        changePlay();
    }

    public void onControl(View view) {
        // 给服务设置播放列表
        Intent intent = new Intent(mContext, MusicService.class);

        if (MusicActivity.notemty) {
            switch (view.getId()) {
                case R.id.iv_previous_music_control:
                    // 设置上一曲指令
                    intent.putExtra(MusicService.KEY_COMMAND, MusicService.CMD_PREV);
                    iv_play_pause.setBackgroundResource(R.drawable.music_pause);
//                code = Command.cmdPrev();
                    break;
                case R.id.iv_next_music_control:
                    // 设置下一曲指令
                    intent.putExtra(MusicService.KEY_COMMAND, MusicService.CMD_NEXT);
                    iv_play_pause.setBackgroundResource(R.drawable.music_pause);
//                code = Command.cmdNext();
                    break;
                case R.id.iv_play_pause:
                    if (MusicUtil.isPlaying) {
                        // 设置暂停
                        intent.putExtra(MusicService.KEY_COMMAND,
                                MusicService.CMD_PAUSE);
                        iv_play_pause.setBackgroundResource(R.drawable.music_play);
                    } else {
                        // 传递播放位置
                        intent.putExtra(MusicService.KEY_MUSIC_INDEX, MusicUtil.index);
                        // 设置播放
                        intent.putExtra(MusicService.KEY_COMMAND,
                                MusicService.CMD_PLAY);
                        iv_play_pause.setBackgroundResource(R.drawable.music_pause);
                    }
                    break;
            }
            // 启动服务
            startService(intent);
        } else {
            Toast.makeText(mContext, "没有可以播放的歌曲", Toast.LENGTH_SHORT).show();
        }
    }


    //初始化sharedPreferences
    public void initsharedPreferences() {
        String NAME = "eight";
        SharedPreferences sharedPreferences = getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (!sharedPreferences.getBoolean("flag", false)) {
            for (int i = 0; i < 8; i++)
                editor.putBoolean("" + i, true);
            editor.putBoolean("flag", true);
        }
        editor.apply();
    }

    //取八个喇叭所在的坐标
    public void getMyImgID() {
        onWindowFocusChanged(true, iV1, 0);
        onWindowFocusChanged(true, iV2, 1);
        onWindowFocusChanged(true, iV3, 2);
        onWindowFocusChanged(true, iV4, 3);
        onWindowFocusChanged(true, iV5, 4);
        onWindowFocusChanged(true, iV6, 5);
        onWindowFocusChanged(true, iV7, 6);
        onWindowFocusChanged(true, iV8, 7);
        mUtils.setPosition(position);
        getStatusBarHeight();
    }

    //根据imgId取相应的坐标
    public void onWindowFocusChanged(boolean hasFocus, View view, int i) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            wh = view.getWidth();
            view.getLocationInWindow(position[i]);
        }
    }

    public void getStatusBarHeight() {
        int result;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
            Utils.setStatusBarHeight(result);
        }
    }


    /**
     * 跳转到设置界面
     */
    private class Setting implements View.OnClickListener {
        public void onClick(View v) {
            Intent intent = null;
            switch (v.getId()) {
                case R.id.bt_setting:
                    intent = new Intent(mContext, SettingActivity.class);
                    break;
                case R.id.iv_music_recond:
                    intent = new Intent(mContext, MusicActivity.class);
//                    scannerMusic();
                    break;
            }
            if (intent != null)
                startActivity(intent);
        }
    }

    class OnTouchRot implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (IdFlag < 1) {
                //只获取一次当前喇叭图片的坐标
                getMyImgID();
                IdFlag++;
            }
            final Ripple ripple = (Ripple) findViewById(R.id.re_main);
            int width;
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                assert ripple != null;
                switch (view.getId()) {
                    case R.id.iV1:
                        width = 0;
                        idFlag1 = !idFlag1;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag1, width);
                        break;
                    case R.id.iV2:
                        width = 1;
                        idFlag2 = !idFlag2;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag2, width);
                        break;
                    case R.id.iV3:
                        width = 2;
                        idFlag3 = !idFlag3;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag3, width);
                        break;
                    case R.id.iV4:
                        width = 3;
                        idFlag4 = !idFlag4;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag4, width);
                        break;
                    case R.id.iV5:
                        width = 4;
                        idFlag5 = !idFlag5;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag5, width);
                        break;
                    case R.id.iV6:
                        width = 5;
                        idFlag6 = !idFlag6;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag6, width);
                        break;
                    case R.id.iV7:
                        width = 6;
                        idFlag7 = !idFlag7;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag7, width);
                        break;
                    case R.id.iV8:
                        width = 7;
                        idFlag8 = !idFlag8;
                        ripple.draw(width, mUtils.getPosition()[width][0], mUtils.getPosition()[width][1], wh);
                        ripple.startRippleAnimation(idFlag8, width);
                        break;
                    default:
                        break;
                }
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                assert ripple != null;
                switch (view.getId()) {
                    case R.id.iV1:
                        width = 0;
                        idFlag1 = !idFlag1;
                        ripple.stopRippleAnimation(idFlag1, width);
                        break;
                    case R.id.iV2:
                        width = 1;
                        idFlag2 = !idFlag2;
                        ripple.stopRippleAnimation(idFlag2, width);
                        break;
                    case R.id.iV3:
                        width = 2;
                        idFlag3 = !idFlag3;
                        ripple.stopRippleAnimation(idFlag3, width);
                        break;
                    case R.id.iV4:
                        width = 3;
                        idFlag4 = !idFlag4;
                        ripple.stopRippleAnimation(idFlag4, width);
                        break;
                    case R.id.iV5:
                        width = 4;
                        idFlag5 = !idFlag5;
                        ripple.stopRippleAnimation(idFlag5, width);
                        break;
                    case R.id.iV6:
                        width = 5;
                        idFlag6 = !idFlag6;
                        ripple.stopRippleAnimation(idFlag6, width);
                        break;
                    case R.id.iV7:
                        width = 6;
                        idFlag7 = !idFlag7;
                        ripple.stopRippleAnimation(idFlag7, width);
                        break;
                    case R.id.iV8:
                        width = 7;
                        idFlag8 = !idFlag8;
                        ripple.stopRippleAnimation(idFlag8, width);
                        break;
                    default:
                        break;
                }
            }

            return true;
        }

    }

    //获取view

    private void initView() {
        mUtils = new Utils();
        position = new int[8][2];
        iv_music_recond = (ImageView) findViewById(R.id.iv_music_recond);
        iv_play_pause = (ImageView) findViewById(R.id.iv_play_pause);
        bt_setting = (ImageButton) findViewById(R.id.bt_setting);
        iV1 = (MyImg) findViewById(R.id.iV1);
        iV2 = (MyImg) findViewById(R.id.iV2);
        iV3 = (MyImg) findViewById(R.id.iV3);
        iV4 = (MyImg) findViewById(R.id.iV4);
        iV5 = (MyImg) findViewById(R.id.iV5);
        iV6 = (MyImg) findViewById(R.id.iV6);
        iV7 = (MyImg) findViewById(R.id.iV7);
        iV8 = (MyImg) findViewById(R.id.iV8);
        seekBar = (SeekBar) findViewById(R.id.seekBar_main);
        textView = (TextView) findViewById(R.id.tv_main_vol);
    }
}