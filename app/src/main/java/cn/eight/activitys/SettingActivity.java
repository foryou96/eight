package cn.eight.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.eight.R;
import cn.eight.utils.ViewHolder;

public class SettingActivity extends AppCompatActivity {
    private ListView listView;

    private SharedPreferences.Editor editor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        init();

        Myada myada = new Myada(this);
        listView.setAdapter(myada);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // 取得ViewHolder对象，这样就省去了通过层层的findViewById去实例化我们需要的cb实例的步骤
                ViewHolder holder = (ViewHolder) view.getTag();
                // 改变CheckBox的状态
                holder.checkBox.toggle();
                // 将CheckBox的选中状况记录下来
                editor.putBoolean("" + i, holder.checkBox.isChecked());
                editor.apply();
            }
        });
    }


    public void back(View view) {
        if (view.getId() == R.id.back_setting)
            finish();
    }

    private void init() {
        listView = (ListView) findViewById(R.id.listView_setting);
        String NAME = "eight";
        SharedPreferences sharedPreferences = getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    class Myada extends BaseAdapter {
        private HashMap<Integer, Boolean> map;
        private LayoutInflater layoutInflater;
        private Context context;

        private final String NAME = "eight";
        private SharedPreferences sharedPreferences;

        private final List<String> list = new ArrayList<String>();

        public Myada(Context context) {
            this.context = context;
            layoutInflater = LayoutInflater.from(this.context);
            list.add(context.getResources().getString(R.string.one));
            list.add(context.getResources().getString(R.string.two));
            list.add(context.getResources().getString(R.string.three));
            list.add(context.getResources().getString(R.string.four));
            list.add(context.getResources().getString(R.string.five));
            list.add(context.getResources().getString(R.string.six));
            list.add(context.getResources().getString(R.string.seven));
            list.add(context.getResources().getString(R.string.eight));
            map = new HashMap<>();
            sharedPreferences = getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        }


        public int getCount() {
            return list.size();
        }

        public Object getItem(int i) {
            return list.get(i);
        }

        public long getItemId(int i) {
            return i;
        }

        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            View view;
            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.layout_listview, null);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.textView.setText(list.get(i));
            viewHolder.checkBox.setChecked(sharedPreferences.getBoolean("" + i, true));
            return view;
        }

        public HashMap<Integer, Boolean> getMap() {
            return map;
        }

        public void setMap(HashMap<Integer, Boolean> map) {
            this.map = map;
        }
    }
}
