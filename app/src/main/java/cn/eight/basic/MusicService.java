package cn.eight.basic;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

public class MusicService extends Service {
    //指令
    public static final String KEY_COMMAND = "k_command";
    //音乐名称
    public static final String KEY_MUSIC_NAME = "k_music_name";
    //音乐路径
    public static final String KEY_MUSIC_PATH = "k_music_path";
    //播放位置
    public static final String KEY_MUSIC_INDEX = "k_music_index";
    //音乐列表
    public static final String KEY_MUSIC_LIST = "k_music_list";
    //总播放时长
    public static final String MUSIC_TIME_TOTAL = "k_time_total";
    //当前播放进度
    public static final String MUSIC_TIME_CURR = "k_time_curr";

    public static final int CMD_INIT = 1000;    //初始化
    public static final int CMD_PLAY = 1001;    //播放
    public static final int CMD_PAUSE = 1002;    //暂停
    public static final int CMD_NEXT = 1003; //下一曲
    public static final int CMD_PREV = 1004;  //上一曲
    public static final int CMD_STOP = 1005; //停止
    public static final int CMD_RESUME = 1006;//从暂停状态恢复

    private Context mContext;
    private int flag = 0;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        //从intent中获取指令
        int command = intent.getIntExtra(KEY_COMMAND, -1);

        //根据不同的指令，调用不同的方法
        switch (command) {
            case CMD_INIT:
                List<String> musicList = intent.getStringArrayListExtra(KEY_MUSIC_LIST);
                if (flag == 0) {
                    new MusicUtil(mContext, musicList);
                    flag++;
                }
                break;
            case CMD_PLAY:
                int index = intent.getIntExtra(KEY_MUSIC_INDEX, 0);
                Log.d("index",index+"");
                Log.d("MusicUtil.index",MusicUtil.index+"");
                if (index == MusicUtil.index) {
                    Log.d("MusicUtil.isPlaying",MusicUtil.isPlaying+"");
                    if (MusicUtil.isPlaying) {
                        MusicUtil.pause();
                    } else {
                        MusicUtil.play();
                    }
                } else {
                    MusicUtil.play(index);
                }
                break;
            case CMD_PAUSE:
                MusicUtil.pause();
                break;
            case CMD_RESUME:
                MusicUtil.play();
                break;
            case CMD_NEXT:
                MusicUtil.next();
                break;
            case CMD_PREV:
                MusicUtil.prev();
                break;
            case CMD_STOP:
                MusicUtil.stop();
                break;
            default:
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }
}
