package cn.eight.basic;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.util.List;

public class MusicUtil {
    private static List<String> musiclist = null;

    public static MediaPlayer mPlayer;// 系统提供的播放器

    private static Context mContext;

    public static int index = 0;
    public static boolean isPlaying = false;// 用于记录播放状态
    private static String musicname;

    public MusicUtil(Context context, List<String> musiclist) {
        mPlayer = new MediaPlayer();
        mContext = context;
        MusicUtil.musiclist = musiclist;
        firstOpen();

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.d("play", "yes");
                next();
            }
        });
    }

    private static void firstOpen() {
        musicname = musiclist.get(MusicUtil.index);
        try {
            mPlayer.setDataSource(mContext, Uri.parse(musicname));
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 播放指定位置的音乐
     *
     * @param index
     */

    public static void play(int index) {
        MusicUtil.index = index;
        mPlayer.reset();
        if (musiclist != null) {
            firstOpen();
            mPlayer.start();
            isPlaying = true;
        }
    }

    /**
     * 暂停方法
     */
    public static void pause() {
        if (mPlayer != null) {
            mPlayer.pause();
            isPlaying = false;
        }
    }

    /**
     * 从暂停状态恢复播放
     */
    public static void play() {
        mPlayer.start();
        isPlaying = true;
    }

    /**
     * 下一曲
     */
    public static void next() {
        if (musiclist != null) {
            if (index == musiclist.size() - 1) {
                index = 0;
            } else {
                ++index;
            }
            play(index);
        }
    }

    /**
     * 上一曲
     */
    public static void prev() {
        if (musiclist != null) {
            if (index == 0) {
                index = musiclist.size() - 1;
            } else {
                --index;
            }
            play(index);
        }
    }

    /**
     * 停止方法
     */
    public static void stop() {
        if (mPlayer != null) {
            mPlayer.stop();
            isPlaying = false;
        }
    }
}
