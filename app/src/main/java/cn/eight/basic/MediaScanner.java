package cn.eight.basic;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

import java.io.File;
import java.io.FilenameFilter;


public class MediaScanner {
    private MediaScannerConnection mConn = null;
    private File mFile = null;
    private String mMimeType = null;

    public MediaScanner(final Context context) {
        SannerClient mClient = null;
        mClient = new SannerClient();
        if (mConn == null) {
            mConn = new MediaScannerConnection(context, mClient);
        }
    }

    public void scanFile(File file, String mimeType) {
        mFile = file;
        mMimeType = mimeType;
        mConn.connect();
    }

    public void disconnect() {
        mConn.disconnect();
    }

    private class FileNameSelector implements FilenameFilter {
        String extension = ".";

        public FileNameSelector(String fileExtensionNoDot) {
            extension += fileExtensionNoDot;
        }

        public boolean accept(File dir, String name) {
            return name.endsWith(extension);
        }
    }

    public class SannerClient implements
            MediaScannerConnection.MediaScannerConnectionClient {
        public void onMediaScannerConnected() {

            if (mFile == null) {
                return;
            }
            scan(mFile, mMimeType);
        }

        public void onScanCompleted(String path, Uri uri) {
            mConn.disconnect();
        }

        public void scan(final File file, final String type) {
            new Thread(new Runnable() {
                public void run() {
                    if (file.isFile()) {
                        mConn.scanFile(file.getAbsolutePath(), null);
                    }
                    File[] files = file.listFiles(new FileNameSelector("flac"));
                    if (files == null) {
                        return;
                    }
                    for (File f : file.listFiles()) {
                        scan(f, type);
                    }
                }
            });
        }
    }
}
